def determine_love():
    name = input("What is your name? ")
    if name.lower() == "alex":
        question1 = input("Does Nathan make you smile? (yes/no) ")
        question2 = input("Do you enjoy messaging ya boy Nathan? (yes/no) ")
        question3 = input("Do you feel a special connection with Nathan? is the the coolest person ever? is Austin the greatest? (yes/no) ")
        
        if question1.lower() == "yes" and question2.lower() == "yes" and question3.lower() == "yes":
            print("Based on your answers, it seems like Nathan loves you!")
        else:
            print("Based on your answers, it doesn't seem like Nathan loves you.")
    else:
        print("Sorry, this program is specifically for Alex.")

# Execute the love determination program
determine_love()
